import './Error404.css';
import { Grid } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import Button from '../../Button/Button';

const ErrorNotFound = () => {
	const history = useHistory();

	return (
		<Grid container direction='column' justify='center' alignItems='center'>
			<h1 style={{ fontSize: '8rem', margin: 0 }}>Oops...</h1>
			<h2 style={{ textAlign: 'center' }}>Sorry, but we couldn't find what you're searching for...</h2>
			<Button text='RETURN TO HOME' handleClick={() => history.push('/')} />
		</Grid>
	);
};

export default ErrorNotFound;
