import './AdminPanel.css';
import { useEffect, useState } from 'react';
import { getAllUsers, searchAllUsers } from '../../services/users.service.js';
import { getToken } from '../../providers/authentication';
import { InputBase } from '@material-ui/core';
import { format } from 'date-fns';
import { ImSearch } from 'react-icons/im';
import { DataGrid } from '@material-ui/data-grid';
import UserUpdate from '../Modals/UserUpdate/UserUpdate';
import Footer from '../Footer/Footer';

const AdminPanel = ({ darkTheme }) => {
	const [users, setUsers] = useState([]);
	const [open, setOpen] = useState(false);
	const [usersEdits, setUsersEdits] = useState(0);
	const [selectedUser, setSelectedUser] = useState({});
	const token = getToken();

	const updateUsersEditsCounter = () => setUsersEdits((prev) => prev + 1);

	const formatDate = (date) => `${format(new Date(date), 'dd-MM-yyyy HH:mm:ss')}`;

	const search = async (keyword) => {
		const foundUsers = await searchAllUsers(keyword, token);
		foundUsers.forEach((u) => {
			u.createdOn = formatDate(u.createdOn);
			u.banExpiry = u.banExpiry ? formatDate(u.banExpiry) : 'not banned';
		});
		setUsers(foundUsers);
	};

	useEffect(() => {
		(async () => {
			const newUsers = await getAllUsers(token);
			newUsers.forEach((u) => {
				u.createdOn = formatDate(u.createdOn);
				u.banExpiry = u.banExpiry ? formatDate(u.banExpiry) : 'not banned';
				if (u.id === selectedUser.id) setSelectedUser(u);
			});
			setUsers(newUsers);
		})();
	}, [token, usersEdits, selectedUser.id]);

	const columns = [
		{ field: 'id', headerName: 'ID', width: 100 },
		{ field: 'username', headerName: 'Username', width: 250 },
		{ field: 'email', headerName: 'Email', width: 250 },
		{ field: 'createdOn', headerName: 'Created on', width: 180 },
		{ field: 'banExpiry', headerName: 'Banned until', width: 180 },
		{ field: 'role', headerName: 'Role', width: 110 },
	];

	return (
		<>
			<div className='admin-panel'>
				<div className='users'>
					<div className='search-bar'>
						<div className='search-icon'>
							<ImSearch />
						</div>
						<InputBase placeholder='Search…' className='search-field' onChange={(e) => search(e.target.value)} defaultValue='' />
					</div>

					<div className='users-table' style={{ width: '100%' }}>
						<DataGrid
							rows={users}
							columns={columns}
							sortModel={[
								{
									field: 'id',
									sort: 'asc',
								},
							]}
							pageSize={20}
							autoHeight={true}
							disableSelectionOnClick={true}
							onCellClick={(params) => {
								setSelectedUser(params.row);
								setOpen(true);
							}}
						/>
					</div>
				</div>
				<UserUpdate
					darkTheme={darkTheme}
					open={open}
					handleClose={() => setOpen(false)}
					userData={selectedUser}
					updateUsersEditsCounter={updateUsersEditsCounter}
				/>
			</div>
			<Footer darkTheme={darkTheme} />
		</>
	);
};

export default AdminPanel;
