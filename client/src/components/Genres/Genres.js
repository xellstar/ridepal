import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '../Button/Button';
import Footer from '../Footer/Footer';
import convertTime from '../../helpers/timeConverter.js';
import { getGenresInfo, getRandomImage } from '../../services/playlists.service.js';
import './Genres.css';
import { getToken } from '../../providers/authentication.js';
import GenreCard from '../GenreCard/GenreCard';
import Loader from '../Loader/Loader';
import getTracks from '../../helpers/getTracks.js';
import generatePlaylist from '../../helpers/generatePlaylist';

const Genres = ({ darkTheme, location }) => {
	const [tripDetails] = useState({
		pointA: location.state?.startLocation || '',
		pointB: location.state?.endLocation || '',
		duration: location.state?.duration || null,
	});
	const [poster, setPoster] = useState(null);
	const [genres, setGenres] = useState([]);
	const [selectedGenres, setGenre] = useState(location.state?.genres || []);
	const [loading, setLoading] = useState(false);
	const [msg, setMsg] = useState('');
	const history = useHistory();
	const token = getToken();

	const tripDuration = convertTime(tripDetails.duration);

	const containerVariants = {
		hidden: {
			opacity: 0,
			x: '100vw',
		},
		visible: {
			opacity: 1,
			x: 0,
			transition: {
				type: 'spring',
				delay: 0.5,
			},
		},
		exit: {
			x: '-100vw',
			transition: { ease: 'easeInOut' },
		},
	};

	const handleClick = async () => {
		if (!selectedGenres.length) setMsg('Select at least 1 genre from the list first !');
		else {
			setLoading(true);

			const tracks = await getTracks(selectedGenres, token);
			const generatedPlaylist = generatePlaylist(tracks, tripDetails.duration, selectedGenres);
			setLoading(false);
			history.push({
				pathname: '/generate/playlist',
				state: {
					duration: tripDetails.duration,
					startLocation: tripDetails.pointA,
					endLocation: tripDetails.pointB,
					genres: selectedGenres,
					imageUrl: poster,
					playlist: { name: '', totalPlaytime: generatedPlaylist.totalPlaytime, trackList: generatedPlaylist.playlist, genres: selectedGenres },
				},
			});
		}
	};

	useEffect(() => {
		(async () => {
			const genresInfo = await getGenresInfo(token);
			const imagePoster = await getRandomImage('music');
			setGenres(genresInfo);
			setPoster(imagePoster.webformatURL);
		})();
	}, []);

	return (
		<>
			<div className='home-wrapper'>
				<motion.div variants={containerVariants} initial='hidden' animate='visible' exit='exit' className='genres-wrapper'>
					<div className='genres-header' style={{ backgroundImage: `url(${poster})` }}>
						<h3>Select up to 3 genres</h3>
						<p className='duration-string'>Trip duration {tripDuration}</p>
						<p>{msg && msg}</p>
					</div>
					<div className={`genres-container${darkTheme ? '-dark' : ''}`}>
						{genres.length &&
							genres.map((genre, i) => {
								return <GenreCard key={i} genre={genre} setGenre={setGenre} selected={selectedGenres} />;
							})}
					</div>
					<div className='genres-form-actions'>
						<Button
							text='BACK'
							handleClick={() =>
								history.push({ pathname: '/generate/routes', state: { startLocation: tripDetails.pointA, endLocation: tripDetails.pointB } })
							}
						/>
						<Button text='NEXT' handleClick={handleClick} />
					</div>
				</motion.div>
			</div>
			{loading && <Loader darkTheme={darkTheme} />}
			<Footer darkTheme={darkTheme} />
		</>
	);
};

export default Genres;
