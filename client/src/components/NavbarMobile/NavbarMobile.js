import './NavbarMobile.css';
import { Link, NavLink } from 'react-router-dom';
import { getUser } from '../../providers/authentication';

const NavbarMobile = ({ logout, darkTheme, toggleDarkTheme, toggleMobileMenu }) => {
	const user = getUser();

	return (
		<div className={darkTheme ? 'mobile-nav mobile-nav-dark' : 'mobile-nav'}>
			{user.username ? (
				<>
					<NavLink
						className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
						activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
						to={'/generate/routes'}
						onClick={toggleMobileMenu}
					>
						Add playlist
					</NavLink>
					<NavLink
						className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
						activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
						to={'/explore'}
						onClick={toggleMobileMenu}
					>
						Explore
					</NavLink>
					{user.role === 'Admin' ? (
						<NavLink
							className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
							activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
							to='/admin-panel'
							onClick={toggleMobileMenu}
						>
							Admin panel
						</NavLink>
					) : (
						''
					)}
					<NavLink
						className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
						activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
						to={`/profile/${user.id}`}
						onClick={toggleMobileMenu}
					>
						My account
					</NavLink>
					<NavLink
						className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
						activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
						to={`/change-password`}
						onClick={toggleMobileMenu}
					>
						Change password
					</NavLink>
					<Link
						className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
						onClick={() => {
							logout();
							toggleMobileMenu();
						}}
						to='/'
					>
						Logout
					</Link>
				</>
			) : (
				<>
					<NavLink
						className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
						activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
						to={'/login'}
						onClick={toggleMobileMenu}
					>
						Login
					</NavLink>
					<NavLink
						className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
						activeClassName={darkTheme ? 'active-link-dark' : 'active-link-light'}
						to={'/register'}
						onClick={toggleMobileMenu}
					>
						Register
					</NavLink>
				</>
			)}

			<span
				className={darkTheme ? 'mobile-link nav-link-dark' : 'mobile-link nav-link-light'}
				onClick={() => {
					toggleMobileMenu();
					toggleDarkTheme();
				}}
			>
				{darkTheme ? 'Light mode' : 'Dark mode'}
			</span>
		</div>
	);
};

export default NavbarMobile;
