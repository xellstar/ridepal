import { Link, NavLink } from 'react-router-dom';
import AvatarComponent from '../Avatar/Avatar';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { RiArrowDropDownLine, RiArrowDropUpLine } from 'react-icons/ri';
import './ProfileNav.css';
import { useState } from 'react';
import { getUser } from '../../providers/authentication.js';
import { makeStyles } from '@material-ui/core';

const ProfileNav = ({ logout, darkTheme }) => {
	const [anchorEl, setAnchorEl] = useState(null);
	const user = getUser();

	const useStyles = makeStyles(() => ({
		menuLight: {
			'& .MuiPaper-root': {
				backgroundColor: '#265a67',
			},
		},
	}));

	const classes = useStyles();

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	return (
		<div className='profile-nav'>
			<div className='controls'>
				<Button aria-controls='simple-menu' aria-haspopup='true' onClick={handleClick} style={{ color: '#ffffff' }}>
					{user.username} {anchorEl ? <RiArrowDropUpLine style={{ fontSize: '16pt' }} /> : <RiArrowDropDownLine style={{ fontSize: '16pt' }} />}
				</Button>
				<Menu
					id='simple-menu'
					anchorEl={anchorEl}
					getContentAnchorEl={null}
					anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
					transformOrigin={{ vertical: 'top', horizontal: 'center' }}
					keepMounted
					open={Boolean(anchorEl)}
					onClose={handleClose}
					className={darkTheme ? '' : classes.menuLight}
				>
					{user.role === 'Admin' ? (
						<MenuItem onClick={handleClose}>
							<NavLink activeClassName='active-link' to='/admin-panel'>
								Admin panel
							</NavLink>
						</MenuItem>
					) : (
						''
					)}

					<MenuItem onClick={handleClose}>
						<NavLink activeClassName='active-link' to={'/generate/routes'}>
							Add playlist
						</NavLink>
					</MenuItem>

					<MenuItem onClick={handleClose}>
						<NavLink activeClassName='active-link' to={`/profile/${user.id}`}>
							My account
						</NavLink>
					</MenuItem>

					<MenuItem onClick={handleClose}>
						<NavLink activeClassName='active-link' to={`/change-password`}>
							Change password
						</NavLink>
					</MenuItem>
					{/* <MenuItem onClick={handleClose}>My account</MenuItem> */}
					<MenuItem
						onClick={() => {
							handleClose();
							logout();
						}}
					>
						<Link to='/'>Logout</Link>
					</MenuItem>
				</Menu>
			</div>
			<div className='avatar-container'>
				<AvatarComponent size='small' />
			</div>
		</div>
	);
};

export default ProfileNav;
