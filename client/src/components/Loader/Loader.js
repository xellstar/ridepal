import { Bars } from '@agney/react-loading';
import './Loader.css';

const Loader = ({ darkTheme }) => {
	return (
		<div className={`loader-container${darkTheme ? '-dark' : ''}`}>
			<Bars className={`loader${darkTheme ? '-dark' : ''}`} />
		</div>
	);
};

export default Loader;
