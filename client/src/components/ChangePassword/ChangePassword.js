import { Button, makeStyles, TextField } from "@material-ui/core";
import { useContext, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useHistory } from "react-router";
import AuthContext, { getToken } from "../../providers/authentication";
import { updateUserPassword } from "../../services/users.service.js";
import { logout } from "../../providers/logout.js";
import Footer from "../Footer/Footer";
import "./ChangePassword.css";

const useStyles = makeStyles(() => ({
    MuiGridFieldDark: {
        backgroundColor: "rgba(0, 0, 0, 0.1)",
        margin: "10px 0",
    },
    MuiGridFieldLight: {
        backgroundColor: "rgba(255, 255, 255, 0.95)",
        margin: "10px 0",
    },
}));

const ChangePassword = ({ darkTheme }) => {
    const { handleSubmit, control, watch } = useForm();
    const [msg, setMsg] = useState("");
    const history = useHistory();
    const classes = useStyles();
    const password = useRef({});
    password.current = watch("password", "");
    const auth = useContext(AuthContext);
    const token = getToken();

    const update = async (formData) => {
        const updatedUser = await updateUserPassword(token, formData.password);

        if (updatedUser.message.includes("successfully")) {
            setMsg(`${updatedUser.message} You will be redirected to login again...`);

            setTimeout(() => {
                logout(token, auth);
                history.push("/login");
            }, 3000);
        } else {
            setMsg(updatedUser.message);
        }
    };

    return (
        <>
            <div className='change-password-root'>
                <div className='change-password-wrapper'>
                    <div className='header'>
                        <h1 style={{ textAlign: "center" }}>Change password</h1>
                        <p>Choose a secure password and don't share it with anyone.</p>{" "}
                        <p>
                            You can find tips for password security{" "}
                            <a
                                href='https://www.liquidweb.com/blog/password-security/'
                                target='_blank'
                                rel='noopener noreferrer'
                            >
                                <b>here.</b>
                            </a>
                        </p>
                    </div>
                    <form className='login-form' onSubmit={(e) => e.preventDefault()}>
                        <Controller
                            name='password'
                            control={control}
                            defaultValue=''
                            rules={{
                                required: "Password is required",
                                minLength: {
                                    value: 8,
                                    message: "Password must have at least 8 characters",
                                },
                                maxLength: {
                                    value: 128,
                                    message: "Password must be no more than 128 characters",
                                },
                            }}
                            render={({ field: { onChange, value }, fieldState: { error } }) => (
                                <TextField
                                    color='secondary'
                                    fullWidth={true}
                                    label='Password'
                                    variant='filled'
                                    value={value}
                                    onChange={onChange}
                                    error={!!error}
                                    helperText={error ? error.message : null}
                                    type='password'
                                    className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
                                />
                            )}
                        />

                        <Controller
                            name='rePassword'
                            control={control}
                            defaultValue=''
                            rules={{
                                validate: (value) => value === password.current || "The passwords do not match",
                            }}
                            render={({ field: { onChange, value }, fieldState: { error } }) => (
                                <TextField
                                    fullWidth={true}
                                    color='secondary'
                                    label='Repeat Password'
                                    variant='filled'
                                    value={value}
                                    onChange={onChange}
                                    error={!!error}
                                    helperText={error ? error.message : null}
                                    type='password'
                                    className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
                                />
                            )}
                        />
                        <p style={{ textAlign: "center", margin: "0" }}>{msg}</p>
                        <Button
                            fullWidth={true}
                            type='submit'
                            variant='contained'
                            color='primary'
                            onClick={handleSubmit((formData) => update(formData))}
                            style={{ marginTop: "10px", padding: "10px" }}
                        >
                            Change password
                        </Button>
                    </form>
                </div>
            </div>
            <Footer darkTheme={darkTheme} />
        </>
    );
};

export default ChangePassword;
