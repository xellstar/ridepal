const generateDescription = (playlist) => {
	if (playlist) {
		const descriptionString = playlist.trackList.reduce((acc, curr, index) => {
			while (index <= 2) {
				if (index === 2 || playlist.trackList.length === index + 1) return (acc += `${curr.title}, and more.`);
				else return (acc += `${curr.title}, `);
			}
			return acc;
		}, `Features `);

		return descriptionString;
	} else {
		return null;
	}
};

export default generateDescription;
