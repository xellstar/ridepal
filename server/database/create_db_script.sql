-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ridepaldb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ridepaldb` DEFAULT CHARACTER SET utf8mb4 ;
USE `ridepaldb` ;

-- -----------------------------------------------------
-- Table `ridepaldb`.`artists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`artists` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `deezerId` INT(11) NOT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `pictureSmall` VARCHAR(255) NOT NULL,
  `pictureMedium` VARCHAR(255) NOT NULL,
  `pictureBig` VARCHAR(255) NOT NULL,
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`deezerId`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`albums`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`albums` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `deezerId` INT(11) NOT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `trackListUrl` VARCHAR(255) NULL DEFAULT NULL,
  `coverSmall` VARCHAR(255) NULL DEFAULT NULL,
  `coverMedium` VARCHAR(255) NULL DEFAULT NULL,
  `coverBig` VARCHAR(255) NULL DEFAULT NULL,
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  `artistId` INT(11) NOT NULL,
  PRIMARY KEY (`deezerId`, `artistId`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `deezerId_UNIQUE` (`deezerId` ASC),
  INDEX `fk_albums_artists1_idx` (`artistId` ASC),
  CONSTRAINT `fk_albums_artists1`
    FOREIGN KEY (`artistId`)
    REFERENCES `ridepaldb`.`artists` (`deezerId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`user_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`user_roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `createdOn` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  `roleId` INT(11) NOT NULL DEFAULT 1,
  `avatar` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `roleId`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_roleId_idx` (`roleId` ASC),
  CONSTRAINT `fk_roleId`
    FOREIGN KEY (`roleId`)
    REFERENCES `ridepaldb`.`user_roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`banned_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`banned_users` (
  `expirationDate` DATETIME NOT NULL,
  `userId` INT(11) NOT NULL,
  PRIMARY KEY (`expirationDate`),
  INDEX `fk_bannedUsers_users_idx` (`userId` ASC),
  CONSTRAINT `fk_bannedUsers_users`
    FOREIGN KEY (`userId`)
    REFERENCES `ridepaldb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`genres` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `deezerId` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `pictureSmall` VARCHAR(255) NOT NULL,
  `pictureBig` VARCHAR(255) NOT NULL,
  `pictureMedium` VARCHAR(255) NOT NULL,
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`deezerId`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `deezerId_UNIQUE` (`deezerId` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`playlists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`playlists` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `userId` INT(11) NOT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `totalPlaytime` INT(11) NULL DEFAULT NULL,
  `rank` INT(11) NULL DEFAULT NULL,
  `createdOn` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  `poster` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `userId`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_playlists_users1_idx` (`userId` ASC),
  CONSTRAINT `fk_playlists_users1`
    FOREIGN KEY (`userId`)
    REFERENCES `ridepaldb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`playlists_genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`playlists_genres` (
  `playlistsId` INT(11) NOT NULL,
  `genreId` INT(11) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  INDEX `fk_playlists_genres_genres1_idx` (`genreId` ASC),
  INDEX `fk_playlistsGenres_playlists1` (`playlistsId` ASC),
  CONSTRAINT `fk_playlistsGenres_playlists1`
    FOREIGN KEY (`playlistsId`)
    REFERENCES `ridepaldb`.`playlists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlists_genres_genres1`
    FOREIGN KEY (`genreId`)
    REFERENCES `ridepaldb`.`genres` (`deezerId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`tags` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`playlists_tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`playlists_tags` (
  `playlistId` INT(11) NOT NULL,
  `tagId` INT(11) NOT NULL,
  PRIMARY KEY (`playlistId`, `tagId`),
  INDEX `fk_playlistsTags_tags1_idx` (`tagId` ASC),
  CONSTRAINT `fk_playlistsTags_playlists1`
    FOREIGN KEY (`playlistId`)
    REFERENCES `ridepaldb`.`playlists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlistsTags_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `ridepaldb`.`tags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`tracks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`tracks` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `deezerId` INT(11) NOT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `duration` INT(11) NULL DEFAULT NULL,
  `rank` INT(11) NULL DEFAULT NULL,
  `previewUrl` VARCHAR(255) NULL DEFAULT NULL,
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  `genreId` INT(11) NOT NULL,
  `albumId` INT(11) NOT NULL,
  `artistId` INT(11) NOT NULL,
  PRIMARY KEY (`deezerId`, `genreId`, `albumId`, `artistId`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `deezerId_UNIQUE` (`deezerId` ASC),
  INDEX `fk_tracks_genres1_idx` (`genreId` ASC),
  INDEX `fk_tracks_albums1_idx` (`albumId` ASC, `artistId` ASC),
  CONSTRAINT `fk_tracks_albums1`
    FOREIGN KEY (`albumId` , `artistId`)
    REFERENCES `ridepaldb`.`albums` (`deezerId` , `artistId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tracks_genres1`
    FOREIGN KEY (`genreId`)
    REFERENCES `ridepaldb`.`genres` (`deezerId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`playlists_tracks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`playlists_tracks` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `playlistId` INT(11) NOT NULL,
  `trackId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_playlists_tracks_tracks1_idx` (`trackId` ASC),
  INDEX `fk_playlistsTracks_playlists1` (`playlistId` ASC),
  CONSTRAINT `fk_playlistsTracks_playlists1`
    FOREIGN KEY (`playlistId`)
    REFERENCES `ridepaldb`.`playlists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlists_tracks_tracks1`
    FOREIGN KEY (`trackId`)
    REFERENCES `ridepaldb`.`tracks` (`deezerId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `ridepaldb`.`tokens_blacklist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ridepaldb`.`tokens_blacklist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(255) NOT NULL,
  `createdOn` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
