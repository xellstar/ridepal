import fetch from 'node-fetch';
import db from '../services/pool.js';
import bcrypt from 'bcrypt';

const fillTracks = async (data, albumId, genreId, artistId) => {
	const SQL = `
        INSERT INTO tracks(deezerId, title, duration, rank, previewUrl, albumId, genreId, artistId)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?);
    `;

	await Promise.all(
		data.map(({ id, title, duration, rank, preview }) => {
			db.query(SQL, [id, title, duration, rank, preview, albumId, genreId, artistId]);
		})
	);
};

const fillAlbums = async (data, artistId, genreId) => {
	const SQL = `
        INSERT INTO albums(deezerId, title, trackListUrl, coverSmall, coverMedium, coverBig, artistId)
        VALUES(?, ?, ?, ?, ?, ?, ?);
    `;

	const SQL2 = `
			SELECT * FROM albums 
			WHERE deezerId = ?
			AND isDeleted = 0
		`;

	await Promise.all(
		data.map(({ id, title, tracklist, cover_small, cover_medium, cover_big }, index) => {
			setTimeout(() => {
				db.query(SQL2, [id]).then((res) => {
					if (res.length) return;
					db.query(SQL, [id, title, tracklist, cover_small, cover_medium, cover_big, artistId]).then((_) => {
						fetch(`https://api.deezer.com/album/${id}/tracks?limit=10`)
							.then((res) => res.json())
							.then(({ data, _ }) => {
								data && fillTracks(data, id, genreId, artistId);
							})
							.catch((err) => console.log(err));
					});
				});
			}, 1000 * index);
		})
	);
};

const fillArtists = async (data, genreId) => {
	const SQL = `
        INSERT INTO artists(deezerId, name, pictureSmall, pictureMedium, pictureBig)
        VALUES(?, ?, ?, ?, ?);
    `;

	const SQL2 = `
			SELECT * FROM artists 
			WHERE deezerId = ?
			AND isDeleted = 0
		`;

	await Promise.all(
		data.map(({ id, name, picture_small, picture_medium, picture_big }, index) => {
			setTimeout(() => {
				db.query(SQL2, [id]).then((res) => {
					if (res.length) return;
					db.query(SQL, [id, name, picture_small, picture_medium, picture_big]).then((_) => {
						fetch(`https://api.deezer.com/artist/${id}/albums?limit=5`)
							.then((res) => res.json())
							.then(({ data, _ }) => {
								data && fillAlbums(data, id, genreId);
							})
							.catch((err) => console.log(err));
					});
				});
			}, 1000 * index);
		})
	);
};

const fillGenres = async (data) => {
	const genres = ['Rock', 'Rap/Hip Hop', 'Electro', 'Jazz', 'Blues'];

	const SQL = `
        INSERT INTO genres(deezerId, name, pictureSmall, pictureMedium, pictureBig)
        VALUES(?, ?, ?, ?, ?);
    `;

	const SQL2 = `
		SELECT * FROM genres 
		WHERE deezerId = ?
		AND isDeleted = 0
	`;

	await Promise.all(
		data.map(({ id, name, picture_small, picture_medium, picture_big }) => {
			if (genres.includes(name)) {
				db.query(SQL2, [id]).then((res) => {
					if (res.length) return;
					db.query(SQL, [id, name, picture_small, picture_medium, picture_big]).then((_) => {
						fetch(`https://api.deezer.com/genre/${id}/artists`)
							.then((res) => res.json())
							.then(({ data, _ }) => {
								data && fillArtists(data, id);
							})
							.catch((err) => console.log(err));
					});
				});
			}
		})
	);
};

const fillDB = () => {
	bcrypt.hash('123123123', 10).then((passHash) => {
		const SQL1 = `
		INSERT INTO user_roles (role)
		VALUES (?)
	`;

		const SQL2 = `
		INSERT INTO users (email, username, password, roleId)
		VALUES (?, ?, ?, ?)
	`;

		console.log('Seed is currently running. Please, stop the seed after 60 seconds with CTRL + C...');

		db.query(SQL1, ['Admin']).then(() => db.query(SQL1, ['User']).then(() => db.query(SQL2, ['admin@example.com', 'Admin', passHash, '1'])));
	});

	fetch('https://api.deezer.com/genre')
		.then((res) => res.json())
		.then(({ data }) => fillGenres(data))
		.catch((err) => console.log(err));
};

fillDB();
