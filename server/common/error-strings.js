const errorStrings = Object.freeze({
	users: {
		username: 'Expected valid string with length [3 - 100].',
		password: 'Expected string with length [8 - 128].',
		email: 'Expected email string with length [5 - 100].',
		role: "Expected role string 'Admin' or 'User'.",
		isDeleted: 'Expected number with value 0 or 1.',
		avatar: 'Expected valid string with length [1-255]',
	},
	playlists: {
		name: 'Expected string with length [3-100].',
		usersId: 'Expected number that is greater than 0 and a valid user id.',
	},
});

export default errorStrings;
