export default (_, res, next) => {
	res.set('API-Version', '1.0');
	next();
};
