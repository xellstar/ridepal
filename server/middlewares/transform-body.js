export default (validator) => (req, _, next) => {
	Object.keys(req.body).forEach((key) => {
		if (!validator[key] || !req.body[key]) delete req.body[key];
	});
	next();
};
