import db from './pool.js';

/**
 * @returns an array of all users, including deleted ones
 */
export const getUsers = async () => {
	const SQL = `
			SELECT u.id, u.username, u.email, u.createdOn, bu.expirationDate as banExpiry, ur.role
			FROM users u 
			LEFT JOIN banned_users bu on u.id = bu.userId
			JOIN user_roles ur on u.roleId = ur.id;
    `;

	try {
		return await db.query(SQL);
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Searches for user in the database by field, including deleted ones
 * @param {string} field
 * @param {string} value
 * @returns found user object
 */
export const getUserBy = async (field, value) => {
	const SQL = `
			SELECT u.id, u.username, u.email, u.createdOn, bu.expirationDate as banExpiry, ur.role
			FROM users u 
			LEFT JOIN banned_users bu on u.id = bu.userId ;
			JOIN user_roles ur on u.roleId = ur.id
			WHERE u.${field} = ?;
    `;

	try {
		const foundUser = await db.query(SQL, [value]);
		return foundUser[0];
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Searches for users whose username starts with the input keyword
 * @param {string} keyword
 * @returns users array
 */
export const getUsersByUsername = async (keyword) => {
	const SQL = `
			SELECT u.id, u.username, u.email, u.createdOn, bu.expirationDate as banExpiry, ur.role
			FROM users u 
			LEFT JOIN banned_users bu on u.id = bu.userId
			JOIN user_roles ur on u.roleId = ur.id
			WHERE u.username LIKE "${keyword}%";
    `;

	try {
		const foundUsers = await db.query(SQL);
		return foundUsers;
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Update a user in the database
 * @param {number} id
 * @param {object} updateData object of fields to be updated
 */
export const updateUser = async (id, updateData) => {
	const data = Object.entries(updateData);
	const SQL = `
        UPDATE users SET
          ${data.map(([key, val]) => `${key} = "${val}"`)}
        WHERE id = ?;
    `;

	const SQL2 = `
		SELECT * FROM users
		WHERE id = ?
	`;

	try {
		await db.query(SQL, [id]);
		const res = await db.query(SQL2, [id]);
		return res[0];
	} catch {
		return null;
	}
};

/**
 * Deletes user by id
 * @param {number} userId
 */
export const deleteUser = async (id) => {
	const SQL = `
		UPDATE users
		SET isDeleted = 1
		WHERE id = ?
	`;

	try {
		return await db.query(SQL, [id]);
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Bans a user by user ID
 * @param {number} userId
 * @param {string} expiration_date
 * @returns ban status object
 */
export const banUser = async (id, expiration_date) => {
	const SQL1 = `
		INSERT INTO banned_users (userId, expirationDate)
		VALUES(?, ?)
	`;

	const SQL2 = `
		SELECT * FROM banned_users
		WHERE userId = ?
	`;

	try {
		await db.query(SQL1, [id, expiration_date]);
		return await db.query(SQL2, [id]);
	} catch {
		return null;
	}
};

/**
 * Unbans a user by user ID
 * @param {number} userId
 */
export const unBan = async (id) => {
	const SQL = `
		DELETE FROM banned_users
		WHERE userId = ?
	`;

	try {
		return await db.query(SQL, [id]);
	} catch {
		return null;
	}
};

/**
 * Returns user ban status by user ID
 * @param {number} userId
 * @returns ban status object
 */
export const getBanStatus = async (id) => {
	const SQL = `
		SELECT * FROM banned_users
		WHERE userId = ?
	`;

	try {
		const banned = await db.query(SQL, [id]);
		return banned[0] || null;
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * @returns a playlist by it's id
 * @param {number} playlist id
 */
export const getPlaylist = async (pid) => {
	const SQL = `
        SELECT id, name, totalPlaytime, rank, createdOn, userId
        FROM playlists
        WHERE id = ?
    `;

	try {
		const playlist = await db.query(SQL, [pid]);
		return playlist[0] || null;
	} catch {
		return null;
	}
};

export const updatePlaylist = async (id, updateData) => {
	const data = Object.entries(updateData);
	const SQL = `
        UPDATE playlists SET
          ${data.map(([key, val]) => `${key} = "${val}"`)}
        WHERE id = ?;
    `;

	const SQL2 = `
		SELECT * FROM playlists
		WHERE id = ?
	`;

	try {
		await db.query(SQL, [id]);
		const res = await db.query(SQL2, [id]);
		return res[0] || null;
	} catch {
		return null;
	}
};

export const deletePlaylist = async (id) => {
	const SQL = `
		UPDATE playlists
		SET isDeleted = 1
		WHERE id = ?
	`;

	try {
		const deleted = await db.query(SQL, [id]);
		return deleted;
	} catch {
		return null;
	}
};
