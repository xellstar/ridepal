import express from 'express';
import { filterTracksBy, getAllTracks, getGenres, getTrackBy } from '../services/tracks.service.js';
import transformQuery from '../middlewares/transform-query.js';

const tracksController = express.Router();

tracksController
	.get('/genres', async (req, res) => {
		const genres = await getGenres();
		return res.status(200).send(genres);
	})

	// get all tracks
	.get('/', transformQuery, async (req, res) => {
		const titleQuery = req.query.title;
		const genreQuery = req.query.genre;
		const albumQuery = req.query.album;
		const artistQuery = req.query.artist;
		const updatedReqQuery = {};
		let tracks;

		Object.entries(req.query).map(([k, v]) => {
			switch (k) {
				case 'title':
					updatedReqQuery['t.title'] = v;
					break;
				case 'genre':
					updatedReqQuery['g.name'] = v;
					break;
				case 'artist':
					updatedReqQuery['artists.name'] = v;
					break;
				case 'album':
					updatedReqQuery['a.title'] = v;
					break;
				case 'order':
					updatedReqQuery['order'] = v;
					break;
				case 'page':
					updatedReqQuery['page'] = v;
					break;
				case 'pageSize':
					updatedReqQuery['pageSize'] = v;
					break;
				default:
					break;
			}
		});

		if (titleQuery || genreQuery || albumQuery || artistQuery) tracks = await filterTracksBy(updatedReqQuery);
		else tracks = await getAllTracks(req.query);

		if (!tracks.length) return res.status(404).send({ message: 'No tracks were found' });
		res.status(200).send(tracks);
	})

	// get track by track id
	.get('/:id', async (req, res) => {
		// todo
		const id = +req.params.id;

		const foundTrack = await getTrackBy('deezerId', id);

		if (!foundTrack) return res.status(404).send({ message: "Track with this id wasn't found" });
		res.status(200).send(foundTrack);
	});

// get all genres

export default tracksController;
