import express from 'express';
import bcrypt from 'bcrypt';
import upload from '../middlewares/upload.js';
import { createPlaylist, deletePlaylist, getUserBy, getUserPlaylists, updatePlaylist, updateUser } from '../services/users.service.js';
import validateBody from '../middlewares/validate-body.js';
import updatePlaylistValidator from '../validators/update-playlist.schema.js';
import createPlaylistValidator from '../validators/create-playlist.schema.js';
import errorStrings from '../common/error-strings.js';

const usersController = express.Router();

usersController

	// update own user password
	.put('/', async (req, res) => {
		const userId = +req.user.id;
		const password = req.body.password;
		const passHash = await bcrypt.hash(password, 10);

		if (password) {
			if (typeof password === 'string' && password.length >= 8 && password.length <= 128) {
				const updatedUser = await updateUser(userId, { password: passHash });

				if (updatedUser && !updatedUser.error) return res.status(200).send({ message: 'Your password was successfully updated!', user: updatedUser });
				else return res.status(400).send({ message: 'Something went wrong... Please try again later.' });
			}
		}
		return res.status(400).send({ message: `Password: ${errorStrings.users.password}` });
	})

	// get user by id
	.get('/:id', async (req, res) => {
		const id = +req.params.id;

		const user = await getUserBy('id', id);

		if (!user) return res.status(404).send({ message: 'User not found.' });
		else return res.status(200).send(user);
	})

	// get all playlists
	.get('/playlists/all', async (req, res) => {
		const genres = req.query.genres?.split(',').map(Number);
		const playlists = await getUserPlaylists(null, null, genres);

		if (playlists.length) return res.status(200).send(playlists);
		else return res.status(404).send({ message: 'Not found.'});
	})

	// get all playlists of a user by user id
	.get('/:id/playlists', async (req, res) => {
		const id = +req.params.id;
		const pid = +req.query.pid || null;

		const user = await getUserBy('id', id);

		if (!user) return res.status(404).send({ message: 'User not found.' });
		else {
			res.status(200).send(await getUserPlaylists(id, pid));
		}
	})

	// create a playlist
	.post('/:id/playlists', validateBody('playlists', createPlaylistValidator), async (req, res) => {
		const userId = +req.params.id;
		const name = req.body.name;
		const tracks = req.body.tracks.split(',');
		const playtime = +req.body.playtime;
		const genres = req.body.genres.split(',');

		const user = await getUserBy('id', userId);

		if (!user) return res.status(404).send({ message: 'User not found.' });
		else {
			// const created = await
			const created = await createPlaylist(userId, name, playtime, tracks, genres);
			if (!created) return res.status(400).send({ message: 'Something went wrong... Please try again later.' });
			else return res.status(200).send({ message: 'Success: playlist created!' });
		}
	})

	// edit own playlist
	.put('/:id/playlists/:pid', validateBody('playlists', updatePlaylistValidator), async (req, res) => {
		const userId = +req.params.id;
		const playlistId = +req.params.pid;
		const newName = req.body.name;

		const user = await getUserBy('id', userId);

		if (!user) return res.status(404).send({ message: 'User not found.' });
		else {
			const playlist = (await getUserPlaylists(userId, playlistId))[0];

			if (!playlist) return res.status(404).send({ message: 'Playlist not found.' });
			else if (playlist.userId !== userId) res.status(403).send({ message: 'Forbidden.' });
			else {
				const updated = await updatePlaylist(playlistId, newName);

				if (!updated) return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
				else return res.status(200).send({ message: `Success: playlist with id ${playlistId} was updated!` });
			}
		}
	})

	// delete own playlist
	.delete('/:id/playlists/:pid', async (req, res) => {
		const userId = +req.params.id;
		const playlistId = +req.params.pid;

		const user = await getUserBy('id', userId);

		if (!user) return res.status(404).send({ message: 'User not found.' });
		else {
			const playlist = await getUserPlaylists(userId, playlistId);

			if (!playlist.length) return res.status(404).send({ message: 'Playlist not found.' });
			else {
				const deleted = await deletePlaylist(playlistId);

				if (!deleted) return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
				else return res.status(200).send({ message: `Success: playlist with id ${playlistId} was deleted!` });
			}
		}
	})

	// upload avatar
	.post('/:id/uploads', upload.single('image'), async (req, res) => {
		const id = +req.params.id;

		const user = await getUserBy('id', id);
		if (!user) return res.status(404).send({ message: 'User not found.' });
		else {
			const updated = await updateUser(id, { avatar: req.file.filename });
			res.status(200).send(updated);
		}
	});

export default usersController;
