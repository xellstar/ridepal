import bcrypt from 'bcrypt';
import express from 'express';
import { authMiddleware, verifyLogin, verifyNotLogged } from '../authentication/auth.js';
import createToken from '../authentication/create-token.js';
import { createUser, getLoginDetailsBy, getUserBy } from '../services/users.service.js';
import { logoutUser } from '../services/auth.service.js';
import transformBody from '../middlewares/transform-body.js';
import validateBody from '../middlewares/validate-body.js';
import registerUserSchema from '../validators/create-user.schema.js';
import loginUserValidator from '../validators/login-user.schema.js';
import { getBanStatus } from '../services/admin.service.js';
import { format } from 'date-fns';

const authController = express.Router();

authController
	// register new user
	.post('/register', transformBody(registerUserSchema), validateBody('users', registerUserSchema), async (req, res) => {
		const { username, password, email } = req.body;
		const foundUser = await getUserBy('username', username);
		const foundEmail = await getUserBy('email', email);

		if (foundUser) return res.status(409).send({ message: 'Username already in use.' });
		if (foundEmail) return res.status(409).send({ message: 'Email already in use.' });
		else {
			const passHash = await bcrypt.hash(password, 10);
			const newUser = await createUser(username, passHash, email);
			res.status(201).send(newUser);
		}
	})

	// login
	.post('/login', transformBody(loginUserValidator), validateBody('users', loginUserValidator), verifyNotLogged, async (req, res) => {
		const { email, password } = req.body;
		const user = await getLoginDetailsBy('email', email);

		if (!user) return res.status(404).send({ message: 'There is no registered user with this email.' });

		const currentBanStatus = await getBanStatus(user.id);

		if (currentBanStatus !== null) {
			if (new Date(currentBanStatus.expirationDate).getTime() > new Date().getTime()) {
				return res
					.status(400)
					.send({ message: `This account has been banned until ${format(new Date(currentBanStatus.expirationDate), 'dd-MMM-yyyy HH:mm:ss.')}` });
			}
		}

		const match = await bcrypt.compare(password, user.password);

		if (!match) return res.status(401).send({ message: 'Wrong username or password.' });
		const token = createToken({ id: user.id, username: user.username, role: user.role });
		res.status(200).set('Authorization', `Bearer ${token}`).send({ message: 'Login successful!', token: token });
	})

	// logout
	.post('/logout', authMiddleware, verifyLogin, async (req, res) => {
		const token = req.headers.authorization.split(' ')[1];
		await logoutUser(token);
		res.status(200).send({ message: 'Logout successful!' });
	});

export default authController;
