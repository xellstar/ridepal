import passport from 'passport';
import { getBanStatus, getUserBy } from '../services/admin.service.js';
import { checkTokenBlacklist, verifyUserToken } from '../services/auth.service.js';

const authMiddleware = passport.authenticate('jwt', { session: false });

const verifyLogin = async (req, res, next) => {
	const token = req.headers.authorization.split(' ')[1];
	const currentBanStatus = await getBanStatus(+req.user.id);

	if (await checkTokenBlacklist(token)) {
		res.status(401).send({ message: 'You need to be logged in to do this.' });
		return;
	}

	if (currentBanStatus !== null) {
		if (new Date(currentBanStatus[0].expirationDate).getTime() > new Date().getTime()) {
			res.status(400).send({ message: `This account has been banned until ${currentBanStatus[0].expirationDate}` });
		}
	} else next();
};

const authAdmin = (req, res, next) => {
	if (req.user && req.user.role === 'Admin') return next();
	res.status(403).send({ message: 'Resource is forbidden.' });
};

const verifyNotLogged = async (req, res, next) => {
	if (!req.headers.authorization) {
		next();
		return;
	}
	const token = req.headers.authorization.split(' ')[1];

	const verifyToken = await verifyUserToken(token);

	if (verifyToken.error) return res.status(403).send({ message: 'Your header authentication token has expired.' });

	const user = await getUserBy('email', req.body.email);

	if (!user) return res.status(401).send({ message: 'Wrong username/password' });

	if (!(await checkTokenBlacklist(token))) return res.status(400).send({ message: 'You are already logged in.' });

	next();
};

export { authMiddleware, authAdmin, verifyLogin, verifyNotLogged };
